const express = require('express');
const path = require('path');
const { v4: uuidV4 } = require('uuid');


const PORT = process.env.PORT || 3000;

const app = express();


app.get('/html', (req, res) => {
    res.status(200).sendFile(path.join(__dirname, 'index.html'), (err) => {
        if(err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.end();
    });
})


app.get('/json', (req, res) => {
    res.status(200).sendFile(path.join(__dirname, 'data.json'), (err) => {
        if(err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.end();
    });
})


app.get('/uuid', (req, res) => {
    res.status(200).json({ "uuid": uuidV4() })
    res.end();
})


app.get('/status/:status_code', (req, res) => {
    const status_code = req.params.status_code;

    try {
        res.sendStatus(status_code);
        res.end();
    } catch (err) {
        res.status(400).json({
            message: "Invalid status code"
        });
    }

})


app.get('/delay/:delay_in_seconds', (req, res) => {
    const delay_in_seconds = parseInt(req.params.delay_in_seconds);
    if(Number.isInteger(delay_in_seconds) && delay_in_seconds >= 0) {
        setTimeout(() => {
            res.sendStatus(200);
            res.end();
        }, delay_in_seconds * 1000);
    }else {
        res.status(400).json({
            message: "Invalid delay"
        });
        res.end();
    }
})

app.use('/', (req, res) => {
    res.sendStatus(404);
    res.end();
})


app.listen(PORT, () => {
    console.log(`listening on the port ${PORT}`);
})